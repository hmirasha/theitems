@extends('layouts.app')

@section('content')

<div class="container ">
			<h1>Add Product</h1>
			<form action="{{route('products.store')}}" method="POST">
			@csrf

			<div class="form-group row">
				<label class="col-sm-2 control-label">
					Name : 
				</label>
			    <div class="col-sm-9">
			    	<input type="text" name="name" class="form-control {{$errors->has('name')?'is-invalid':''}}" autocomplete="off" value="{{old('name')}}">
			    </div>
			</div>
			@if($errors->has('name'))
			    <div class="invalid-feedback">
			        <strong>{{$errors->first('name')}}</strong>
			    </div>
		    @endif

			<div class="form-group row">
				<label class="col-sm-2 control-label">
					Description : 
				</label>
			    <div class="col-sm-9">
			    	<textarea rows="10" name="description" class="form-control {{$errors->has('description')?'is-invalid':''}}" autocomplete="off" value="{{old('description')}}">{{old('description')}}</textarea>
			    </div>
			</div>
			@if($errors->has('description'))
			    <div class="invalid-feedback">
			        <strong>{{$errors->first('description')}}</strong>
			    </div>
		    @endif
	

			<div class="form-group row">
				<label class="col-sm-2 control-label">
					Quantity	 : 
				</label>
			    <div class="col-sm-9">
			    	<input type="text" name="quantity" class="form-control {{$errors->has('quantity')?'is-invalid':''}}" autocomplete="off">
			    </div>
			</div>
			@if($errors->has('quantity'))
			    <div class="invalid-feedback">
			        <strong>{{$errors->first('quantity')}}</strong>
			    </div>
		    @endif

		    <div class="form-group row">
		      <label class="col-sm-2  form-check-label">
		           Status :
		       </label>
		      <div class="col-sm-6">    
		         <select class="form-control form-block" name="status">
		          <option value="0">Select Status</option>
		          <option value="good">Good</option>
		          <option value="used">Used</option>
		          <option value="old">Old</option>
		         </select> 
		        </div>
		       </div>

			 <div class="form-group row">
			    <label class="col-sm-2  form-check-label">Sku :</label>
			     <div class="col-sm-9">
			    	<input type="text" name="sku" class="form-control {{$errors->has('sku')?'is-invalid':''}}" autocomplete="off">
			    </div>
			  </div>

			  @if($errors->has('sku'))
			    <div class="invalid-feedback">
			        <strong>{{$errors->first('sku')}}</strong>
			    </div>
			   @endif

			  	<div class="form-group row">
					<label class="col-sm-2 control-label">
						Price : 
					</label>
				    <div class="col-sm-9">
				    	<input type="text" name="price" class="form-control {{$errors->has('price')?'is-invalid':''}}" autocomplete="off">
				    </div>
				</div>
				@if($errors->has('price'))
			    <div class="invalid-feedback">
			        <strong>{{$errors->first('price')}}</strong>
			    </div>
			   @endif

				  <div class="form-group row ">
				    	<button type="submit" name="save" class="btn btn-info">Add Product</button>
				</div>
			</form>
            <hr>
			|<a href="/products"> Back to Products</a> |
		</div>
@endsection