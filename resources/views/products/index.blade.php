@extends('layouts.app')

@section('content')
<div class="container">
     <div >
          <a href="{{route('products.create')}}" class="btn btn-outline-primary btn-sm float-right">Add Product</a>
     </div>
     <br>
     <br>
	<table class="table">
		<thead> 
			<th>ID</th>
			<th>Name</th>
			<th>Description</th>
			<th>Quantity</th>
			<th>Sku</th>
			<th>Status</th>
			<th>Price</th>
			<th>Options</th>
		</thead>
		@if(empty($products))
		<tr>
			<td colspan="8">Sorry No Products </td>
		</tr>
		@else
          @foreach($products as $product)
          <tr>
          	<td>
          	   {{$product->id}}
          	</td>
          	<td>
          	  <a href="{{route('products.show',$product->id)}}">{{$product->name}}</a> 
          	</td>
          	<td>
          	   {{$product->description}}
          	</td>
          	<td>
          	   {{$product->quantity}}
          	</td>
          	<td>
          	   {{$product->sku}}
          	</td>
          	<td>
          	   {{$product->status}}
          	</td>
          	<td>
          	   {{$product->price}}
          	</td>
          	<td>
          		<a href="{{route('products.edit',$product->id)}}" class="btn btn-outline-info btn-sm">Edit</a>
          		<form method="POST" action="{{route('products.destroy',$product->id)}}" class="delProduct">
          			@csrf
          			@method('DELETE')
          			<input type="submit" name="Delete" value="Delete" class="btn btn-outline-danger btn-sm">
          		</form>
          	</td>
          </tr>
          @endforeach
		@endif
	</table>
</div>

@endsection