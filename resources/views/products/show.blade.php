@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-body">
		<h3>{{$product->name}}</h3>
		<table class="table text-center">
			<tr>
				<td>
					{{$product->id}}
				</td>
			</tr>
			<tr>
				<td>
					{{$product->description}}
				</td>
			</tr>
			<tr>
				<td>
					{{$product->quantity}}
				</td>
			</tr>
			<tr>
				<td>
					{{$product->sku}}
				</td>
			</tr>
			<tr>
				<td>
					{{$product->status}}
				</td>
			</tr>
			<tr>
				<td>
					{{$product->price}}
				</td>
			</tr>
			<tr>
				<td>
					<a href="{{route('products.edit',$product->id)}}" class="btn btn-outline-info btn-sm">Edit</a>
	          		<form method="POST" action="{{route('products.destroy',$product->id)}}" class="delProduct">
	          			@csrf
	          			@method('DELETE')
	          			<input type="submit" name="Delete" value="Delete" class="btn btn-outline-danger btn-sm">
	          		</form>
				</td>
			</tr>
		</table>
	</div>
</div>
@endsection