<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
	        'name'=>$faker->word,
	        'description'=>$faker->paragraph(1),
	        'quantity'=>rand(0,10),
	        'sku'=>$faker->word,
	        'status'=>$faker->randomElement(['Good','Used','Old']),
	        'price'=>rand(1,100),
	  ];
});
