<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('products')->delete();
    	DB::table('users')->delete();
      //  $this->call(UsersTableSeeder::class);
       factory(App\Product::class, 100)->create();
    }
}
